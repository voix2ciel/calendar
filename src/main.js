import Vue from "vue";
import App from "./App.vue";
import store from "./store";
import router from "./router";
import moment from "moment";

import "fullcalendar/dist/fullcalendar.css";

window.moment = moment;

new Vue({
  el: "#app",
  store,
  router,
  render: h => h(App)
});
