import VueRouter from "vue-router";
import Vue from "vue";
import Main from "../components/Main";

Vue.use(VueRouter);

const routes = [
  {
    name: "Main",
    path: "/",
    component: Main
  }
];

const router = new VueRouter({
  routes
});

export default router;
