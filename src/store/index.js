import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    showForm: {
      visible: false,
      edit: false
    },
    events: [],
    showInfo: {
      visible: false,
      desc: ''
    },
    currentData: {
      date: '',
      desc: '',
      title: ''
    }
  },
  getters: {
    showForm(state) {
      return state.showForm;
    },
    events(state) {
      return state.events;
    },
    showInfo(state) {
      return state.showInfo;
    },
    currentData(state) {
      return state.currentData;
    }
  },
  mutations: {
    showForm(state, {visible, edit}) {
      state.showForm = {visible, edit};
    },
    showInfo(state, {desc, visible}) {
      state.showInfo = {visible, desc};
    },
    addEvent(state, {event}) {
      state.events.push(event);
    },
    removeEvent(state, {id}) {
      let index = state.events.findIndex(item => {
        if (item.id === id) {
          return true;
        }
      });
      state.events.splice(index, 1);
    },
    updateEvent(state, {id, event}) {
      let index = state.events.findIndex(item => {
        if (item.id === id) {
          return true;
        }
      });
      localStorage.removeItem(id);
      localStorage.setItem(event.id, JSON.stringify(event));
      state.events.splice(index, 1, event);
    },
    setCurrentData(state, {event}) {
      state.currentData = event;
    }
  },
  actions: {
    showForm({commit}, {visible, edit = false}) {
      if (!visible) {
        const event = {title: '', date: '', id: '', desc: ''};
        commit('setCurrentData', {event});
      }
      commit('showForm', {visible, edit});
    },
    showInfo({commit}, {desc, visible}) {
      commit('showInfo', {desc, visible});
    },
    addEvent({commit}, {event}) {
      localStorage.setItem(event.id, JSON.stringify(event));
      commit('addEvent', {event});
    },
    removeEvent({commit}, {id}) {
      localStorage.removeItem(id);
      commit('removeEvent', {id});
    },
    updateEvent({commit}, {id, event}) {
      commit('updateEvent', {id, event});
    },
    setCurrentData({commit}, {event}) {
      commit('setCurrentData', {event});
    }
  }
});

export default store;
